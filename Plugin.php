<?php namespace Tripplefix\EventManager;

use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Tripplefix\EventManager\Components\InvitationForm' => 'invitationForm',
            'Tripplefix\EventManager\Components\Unsubscribe' => 'unsubscribe'
        ];
    }

    public function registerSettings()
    {
    }

    public function registerMailTemplates()
    {
        return [
            'tripplefix.eventmanager::emails.invitation' => 'Einladungs Mail (Das erste Mail mit dem Link zur Einladung)',
            'tripplefix.eventmanager::emails.pursuit' => 'Erinnerungs Mail (wird an diejenigen versendet, die nicht auf die Einladung reagiert haben)',
            'tripplefix.eventmanager::emails.reminder' => 'Erinnerungs Mail (Wird an alle versendet, welche die Einladung angenommen haben)',
            'tripplefix.eventmanager::emails.followup' => 'Follow Up Mail (wird an diejenigen versendet, die am Event teilgenommen haben)',
            'tripplefix.eventmanager::emails.feedback' => 'Feedback Mail',
        ];
    }

    public function registerNavigation()
    {
        return [
            'event_manager' => [
                'label'       => 'tripplefix.eventmanager::lang.plugin.name',
                'url'         => Backend::url('tripplefix/eventmanager/events'),
                'icon'        => 'icon-beer',
                'iconSvg'     => 'plugins/tripplefix/eventmanager/assets/images/eventmanager-icon.svg',
                'permissions' => ['tripplefix.event_manager.manage_events'],
                'order'       => 900,

                'sideMenu' => [
                    'event_list' => [
                        'label'       => 'tripplefix.eventmanager::lang.event_list',
                        'icon'        => 'icon-th-list',
                        'url'         => Backend::url('tripplefix/eventmanager/events'),
                        'permissions' => ['tripplefix.event_manager.manage_events']
                    ]
                ]
            ],
            'customer_manager' => [
                'label'       => 'tripplefix.eventmanager::lang.customer_list',
                'icon'        => 'icon-group',
                'iconSvg'     => 'plugins/tripplefix/eventmanager/assets/images/customer-manager-icon.png',
                'url'         => Backend::url('tripplefix/eventmanager/customers'),
                'order'       => 901,
            ]
        ];
    }

    public function registerSchedule($schedule)
    {
        $schedule->command('queue:listen')->everyMinute()->withoutOverlapping();
    }
}
