<?php namespace Tripplefix\EventManager\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use Tripplefix\EventManager\Models\Invitation;
use Tripplefix\EventManager\Models\Event;
use Mail;

class Unsubscribe extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Abmeldung vom Newsletter',
            'description' => 'Meldet denjenigen, der die Seite aufruft ab'
        ];
    }

    public function defineProperties()
    {
        return [
            'unsubscribeType' =>[
                'title' => 'Abmelde Art',
                'description' => '',
                'default' => 'event-mails',
                'type' => 'dropdown',
                'placeholder' => 'Wähle aus ...',
                'options' => ['event-mails' => 'Nur Event-E-Mails', 'all-mails' => 'Alle VIU-E-Mails']
            ],
            'event' => [
                'title'             => 'Event',
                'description'       => 'Wähle den Event aus, für den du die Einladung anzeigen willst.',
                'type'              => 'dropdown',
                'required'          => 'true',
                'validationMessage' => 'Bitte wähle etwas aus'
            ]
        ];
    }

    public function getEventOptions(){
        $events = array();

        foreach (Event::all() as $event) {
            $events[$event->id] = $event->name;
        }

        return $events;
    }

    public function onRun(){
        //only load the site when the user uses a valid key. Else log EVERYTHING
        $invitation = Invitation::keynumber(get('e'))->event($this->property('event'))->firstOrFail();

        $customer = $invitation->customer;

        if($this->property('unsubscribeType') == 'event-mails'){
            $customer->unsubscribed_eventmails = true;
        }else{
            $customer->unsubscribed_viumails = true;
        }
        $customer->save();
    }
}