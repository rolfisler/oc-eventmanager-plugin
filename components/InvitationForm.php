<?php namespace Tripplefix\EventManager\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;
use Illuminate\Support\Facades\DB;
use Tripplefix\EventManager\Models\Invitation;
use Tripplefix\EventManager\Models\Event;
use Mail;
use Log;
use Flash;

class InvitationForm extends ComponentBase
{
    public $visitor;

    public function componentDetails()
    {
        return [
            'name'        => 'Invitation form',
            'description' => 'Displays a form for an invitation for a selected event'
        ];
    }

    public function defineProperties()
    {
        return [
            'event' => [
                'title'             => 'Event',
                'description'       => 'Wähle den Event aus, für den du die Einladung anzeigen willst.',
                'type'              => 'dropdown',
                'required'          => 'true',
                'validationMessage' => 'Bitte wähle etwas aus'
            ]
        ];
    }

    public function getEventOptions(){
        $events = array();

        foreach (Event::all() as $event) {
            $events[$event->id] = $event->name;
        }

        return $events;
    }

    public function onRun(){

        $this->visitor = Invitation::keynumber(get('e'))->event($this->property('event'))->firstOrFail();

        if($this->visitor !== null){

            //how many are coming to the event?
            $commitments = Invitation::whereIn('state_id', [6,7,8,9,10])->count();

            //to make the number more impressive, we add the accompaniments
            //$accompaniments = IM::where('accompaniment', 1)->count();
            $this->page[ 'commitments' ] = $commitments; // + $accompaniments;
            $this->page[ 'event_id' ] = $this->property('event'); // + $accompaniments;

            $state_id = $this->visitor->state_id;

            // update state
            if($state_id == 2){
                $this->visitor->state_id = 3;
                $this->visitor->save();
            }else if($state_id == 4){
                $this->visitor->state_id = 5;
                $this->visitor->save();
            }else if($state_id == 7){
                $this->visitor->state_id = 8;
                $this->visitor->save();
            }else if($state_id == 9){
                $this->visitor->state_id = 10;
                $this->visitor->save();
            }
        }else{
            try{
                // log user data
                $user_ip = '';
                $forwarded_for = '';
                $country = '';
                $city = '';
                $user_agent = '';

                if(isset($_SERVER['REMOTE_ADDR'])){
                    $remote_addr = $_SERVER['REMOTE_ADDR'];

                    $user_ip = getenv('REMOTE_ADDR');
                    $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));

                    $country = $geo["geoplugin_countryName"];
                    $city = $geo["geoplugin_city"];
                }

                if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
                    $forwarded_for = $_SERVER['HTTP_X_FORWARDED_FOR'];
                }

                if(isset($_SERVER['HTTP_USER_AGENT'])){
                    $user_agent = $_SERVER['HTTP_USER_AGENT'];
                }

                //viu_unauthorized_access_list
                Db::table('tripplefix_eventmanager_unauthorized_access_list')->insert([
                    'parameter_val' => is_null(get('e')) ? '' : get('e'),
                    'ip' => $user_ip,
                    'forwarded_ip' => $forwarded_for,
                    'country' => $country,
                    'city' => $city,
                    'user_agent' => $user_agent
                ]);
            }catch (\Exception $ex) {

                //$this->page[ 'error' ] = $ex;
            }finally{
                throw new \ApplicationException('You cannot access this website without a code / without the correct code');
            }
        }
    }
}
