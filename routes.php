<?php

use Illuminate\Http\Request;
use Tripplefix\EventManager\Classes\InvitationUtils as IU;

Route::post('/pushResponse', function(Request $request)
{
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, TRUE); //convert JSON into array

    if(($key = $input['key']) &&
        ($eventId = $input['event_id']) &&
        ($answer = $input['answer'])) {
        return IU::processAnswer($key, $eventId, $answer);
    }else{
        return [
            'customError' => 'invalid parameters'
        ];
    }
});

Route::post('/changeName', function()
{
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, TRUE); //convert JSON into array

    if(($key = $input['key']) &&
        ($eventId = $input['event_id']) &&
        ($firstname = $input['firstname']) &&
        ($lastname = $input['lastname']) &&
        ($email = $input['email'])) {
        return IU::changeName($key, $eventId, $firstname, $lastname, $email);
    }else{
        return [
            'customError' => 'invalid keynumber'
        ];
    }
});
