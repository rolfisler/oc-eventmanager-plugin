<?php namespace Tripplefix\EventManager\Models;

use Model;

/**
 * Model
 */
class Invitation extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'event' => [
            'Tripplefix\EventManager\Models\Event',
            'key'      => 'event_id'
        ],
        'customer' => [
            'Tripplefix\EventManager\Models\Customer',
            'key'      => 'customer_id'
        ],
        'state' => [
            'Tripplefix\EventManager\Models\InvitationState',
            'key'      => 'state_id'
        ]
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'tripplefix_eventmanager_invitations';

    public function scopeEvent($query, $eventId)
    {
        return $query->where('event_id', $eventId);
    }

    public function scopeKeynumber($query, $keynumber)
    {
        $customer = Customer::where('keynumber', $keynumber)->first();
        return $query->where('customer_id', $customer->id);
    }
}
