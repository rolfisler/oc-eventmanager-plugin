<?php namespace Tripplefix\EventManager\Models;

use Model;
use BackendAuth;
use Illuminate\Support\Facades\DB;

/**
 * Model
 */
class Customer extends Model
{
    use \October\Rain\Database\Traits\Validation;

    protected $guarded = ['keynumber'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'invitations' => 'Tripplefix\EventManager\Models\Invitation'
    ];

    public $belongsToMany = [
        'events' => ['Tripplefix\EventManager\Models\Event', 'table' => 'tripplefix_eventmanager_invitations']
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tripplefix_eventmanager_customers';

    /**
     * Generate a random hash keynumber
     */
    public function beforeCreate()
    {
        $this->keynumber = md5($this->email . 'viu4you');

        $user = BackendAuth::getUser();
        $this->viu_contact = $user->first_name . ' ' . $user->last_name;
    }

    public function scopeShowMine($query)
    {
        $user = BackendAuth::getUser();
        return $query->where('viu_contact', $user->first_name . ' ' . $user->last_name);
    }
}
