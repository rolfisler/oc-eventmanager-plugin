<?php namespace Tripplefix\EventManager\Models;

use Model;
use Tripplefix\EventManager\Models\Customer as CM;

/**
 * Model
 */
class Event extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'invitations' => 'Tripplefix\EventManager\Models\Invitation'
    ];

    public $belongsToMany = [
        'customers' => ['Tripplefix\EventManager\Models\Customer', 'table' => 'tripplefix_eventmanager_invitations']
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tripplefix_eventmanager_events';

    public $add_all;

    /**
     * Generate a random hash keynumber
     */
    public function afterCreate()
    {
        if($this->add_all){
            $ids = CM::where('unsubscribed_eventmails', 0)->where('unsubscribed_viumails', 0)->pluck('id')->all();
            $this->customers()->attach($ids);
        }
    }
}
