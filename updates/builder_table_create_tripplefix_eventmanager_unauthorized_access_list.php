<?php namespace Tripplefix\EventManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTripplefixEventmanagerUnauthorizedAccessList extends Migration
{
    public function up()
    {
        Schema::create('tripplefix_eventmanager_unauthorized_access_list', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('parameter_val', 255)->nullable();
            $table->string('ip', 255)->nullable();
            $table->string('forwarded_ip', 255)->nullable();
            $table->string('country', 255)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('user_agent', 2048)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tripplefix_eventmanager_unauthorized_access_list');
    }
}
