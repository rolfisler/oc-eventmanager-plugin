<?php namespace Tripplefix\EventManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTripplefixEventmanagerEvents extends Migration
{
    public function up()
    {
        Schema::create('tripplefix_eventmanager_events', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->date('date');
            $table->time('time_start');
            $table->time('time_end');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tripplefix_eventmanager_events');
    }
}
