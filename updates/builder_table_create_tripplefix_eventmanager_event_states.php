<?php namespace Tripplefix\EventManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTripplefixEventmanagerEventStates extends Migration
{
    public function up()
    {
        Schema::create('tripplefix_eventmanager_event_states', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('value', 255);
            $table->string('text', 255);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tripplefix_eventmanager_event_states');
    }
}
