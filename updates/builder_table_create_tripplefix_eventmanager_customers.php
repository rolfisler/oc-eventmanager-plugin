<?php namespace Tripplefix\EventManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTripplefixEventmanagerCustomers extends Migration
{
    public function up()
    {
        Schema::create('tripplefix_eventmanager_customers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email', 255);
            $table->string('firstname', 255);
            $table->string('lastname', 255);
            $table->string('company', 255)->nullable();
            $table->string('sex', 1);
            $table->smallInteger('is_formal');
            $table->string('viu_contact', 255);
            $table->string('keynumber', 255);
            $table->smallInteger('unsubscribed_eventmails')->default(0);;
            $table->smallInteger('unsubscribed_viumails')->default(0);;
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tripplefix_eventmanager_customers');
    }
}
