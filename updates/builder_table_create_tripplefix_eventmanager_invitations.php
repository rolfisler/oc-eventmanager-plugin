<?php namespace Tripplefix\EventManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTripplefixEventmanagerInvitations extends Migration
{
    public function up()
    {
        Schema::create('tripplefix_eventmanager_invitations', function($table)
        {
            $table->increments('id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->integer('state_id')->default(1);
            $table->smallInteger('changed_name')->default(0);
            $table->text('personal_message')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tripplefix_eventmanager_invitations');
    }
}
