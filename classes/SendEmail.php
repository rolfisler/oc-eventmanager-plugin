<?php namespace  Tripplefix\EventManager\Classes;

use Pheanstalk\Exception;
use Tripplefix\EventManager\Models\Invitation as IM;
use Mail;
use Log;
use Flash;

class SendEmail
{
    public function fire($job, $data){

        if ($job->attempts() > 1) {
            Log::error('Job stopped after 3 attempts. Reason unknown.');
            $job->delete();
        }

        // invitee object
        $invitation = IM::find($data["invitation_id"]);

        // check if customer is subscribed
        if($invitation->customer->unsubscribed_eventmails || $invitation->customer->unsubscribed_viumails){
            Log::warning('Aborted dispatch to ' . $invitation->customer->email . ' because he/she has opted out of the list.');
            $job->delete();
        }

        // preserve old state, in case something goes wrong
        $old_state = $invitation->state_id;

        // set mail-template and new state depending on the type of mail
        $mail_template = 'tripplefix.eventmanager::emails.';
        $new_state = 0;
        switch ($data["type"]){
            case 'invitation':
                $mail_template .= 'invitation';
                $new_state = 2;
                break;
            case 'pursuit':
                $mail_template .= 'pursuit';
                $new_state = 4;
                break;
            case 'reminder':
                $mail_template .= 'reminder';
                $new_state = 7;
                break;
            case 'followup':
                $mail_template .= 'followup';
                $new_state = 9;
                break;
            default:
                Log::error('Failed to send mail to ' . $invitation->customer->email . '. Type not fount: ' . $data["type"]);
                $job->delete();
                break;
        }

        // try to send the email
        try {
            Mail::send($mail_template, $this->setMaildata($invitation), function ($message) use ($invitation, $new_state) {
                $message->to($invitation->customer->email, $invitation->customer->firstname . ' ' . $invitation->customer->lastname);
                $message->attach('storage/app/media/high-five.ics');
                $invitation->state_id = $new_state;
                $invitation->save();
            });
            $job->delete();
        } catch(Exception $e) {
            if ($job->attempts() > 3) {
                $invitation->state_id = $old_state;
                $invitation->save();
                Log::error('Failed to send mail after three attempts for: '. $invitation->email . '. Will delete job. Error: ' . $e);
                $job->delete();
            }else{
                Log::warning('Failed to send mail to: '. $invitation->email . '. Will try again. Error: ' . $e);
                $job->release();
            }
        }
    }

    /**
     * Sets personal mail-date such as salutation and personal key-number
     * @param $invitee
     * @return array
     */
    private function setMaildata($invitation){
        $formal_m = 'Sehr geehrter Herr ';
        $formal_f = 'Sehr geehrte Frau ';
        $informal_m = 'Lieber ';
        $informal_f = 'Liebe ';

        if($invitation->customer->is_formal){
            $du_sie = 'Sie';
            $dich_sie = 'Sie';
            $deine_ihre = 'Ihre';
            $hast_haben = 'haben';

            $salutation = $invitation->customer->sex == 'm'
                ? $formal_m . $invitation->customer->firstname
                : $formal_f . $invitation->customer->firstname;
        }else{
            $du_sie = 'Du';
            $dich_sie = 'Dich';
            $deine_ihre = 'Deine';
            $hast_haben = 'hast';

            $salutation = $invitation->customer->sex == 'm'
                ? $informal_m . $invitation->customer->firstname
                : $informal_f . $invitation->customer->firstname;
        }

        return [
            'key' => $invitation->customer->keynumber,
            'salutation' => $salutation,
            'du_sie' => $du_sie,
            'dich_sie' => $dich_sie,
            'deine_ihre' => $deine_ihre,
            'hast_haben' => $hast_haben,
        ];
    }
}