<?php namespace Tripplefix\EventManager\Classes;

use Tripplefix\EventManager\Models\Invitation;

class InvitationUtils
{
    public static function processAnswer($key, $eventId, $answer){

        if($invitation = Invitation::keynumber($key)->event($eventId)->firstOrFail()){

            if($answer == 'yes'){
                $invitation->state_id = 6;
            }else if($answer == 'maybe'){
                $invitation->state_id = 13;
            }else if($answer == 'no'){
                $invitation->state_id = 11;
            }else{
                return [
                    'customError' => 'invalid choice'
                ];
            }
            $invitation->save();

            return $invitation->customer;
        }else{
            return [
                'customError' => 'invalid keynumber'
            ];
        }
    }

    public static function changeName($key, $eventId, $firstname, $lastname, $email){

        if($invitation = Invitation::keynumber($key)->event($eventId)->firstOrFail()){

            $customer = $invitation->customer;

            if($customer->firstname != $firstname || $customer->lastname != $lastname || $customer->email != $email){
                $invitation->changed_name = 1;

                if($firstname != ''){
                    $customer->firstname = $firstname;
                }
                if($lastname != ''){
                    $customer->lastname = $lastname;
                }
                if($email != ''){
                    $customer->email = $email;
                }
                $customer->save();
            }
            $invitation->save();

            return $customer;
        }else{
            return [
                'customError' => 'invalid keynumber'
            ];
        }
    }
}