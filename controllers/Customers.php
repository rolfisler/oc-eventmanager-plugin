<?php namespace Tripplefix\EventManager\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;

class Customers extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Tripplefix.EventManager', 'customer_manager');
    }

    public function listExtendQuery($query)
    {
        if(!$this->user->hasAccess('tripplefix.event_manager.manage_customers')){
            $user = BackendAuth::getUser();
            $query->where('viu_contact', $user->first_name . ' ' . $user->last_name);
        }
    }
}
