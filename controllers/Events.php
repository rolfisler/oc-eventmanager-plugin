<?php namespace Tripplefix\EventManager\Controllers;

use Flash;
use Mail;
use Log;
use Queue;
use BackendMenu;
use Pheanstalk\Exception;
use Backend\Classes\Controller;
use Tripplefix\EventManager\Models\Event as EM;
use Tripplefix\EventManager\Models\Customer as CM;
use Tripplefix\EventManager\Models\Invitation as IM;

class Events extends Controller
{
    public $event_name;

    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend.Behaviors.RelationController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'tripplefix.event_manager.manage_events' 
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Tripplefix.EventManager', 'event_manager');
    }

    public function dispatch($event_id){
        $invitees = IM::with('customer')->where('event_id', $event_id)->whereHas('customer', function ($query) {
            $query->where('email', 'not like', '%@viu.ch');
        })->get();

        $this->vars['answered'] =   $invitees->whereIn('state_id', [6,7,8,9,10,11,12,13])->count();
        $this->vars['no_answer'] =  $invitees->whereIn('state_id', [2,3,4,5])->count();

        $this->vars['accepted'] =   $invitees->whereIn('state_id', [6,7,8,9,10,12])->count();
        $this->vars['declined'] =   $invitees->whereIn('state_id', [11])->count();
        $this->vars['maybe'] =      $invitees->whereIn('state_id', [13])->count();
        $this->vars['new_guests'] = $invitees->whereIn('state_id', [1])->count();

        try{
            $this->bodyClass = 'slim-container';
            $event = EM::findOrFail($event_id);
            $this->initRelation($event);
            $this->vars['event_name'] = $event->value('name');
        }catch(Exception $e){
            return Redirect::to(Url::to('index'));
        }
    }

    /**
     * Sending Mail
     * */
    public function onSendAll($event_id){
        switch (post('type')){
            case 'invitation':
                $states = [1];
                break;
            case 'pursuit':
                $states = [2,3];
                break;
            case 'reminder':
                $states = [6,13];
                break;
            case 'followup':
                $states = [12];
                break;
            default:
                $states = [];
                break;
        }

        $invitations = IM::where('event_id', $event_id)->whereIn('state_id', $states)->get();
        $invitations_count = $invitations->count();

        if(!$invitations_count){
            Flash::warning('Die Auswahl trifft auf keine Gäste zu. Es wurden keine E-Mails versendet');
        }else{
            try{
                foreach ($invitations as $invitation) {
                    $this->pushMail($invitation->id, post('type'));
                }
            } catch(Exception $e) {
                Log::warning('Error while sending E-Mail. Error: ' . $e);
                /*if($failcount = IM::where('state_id', 1)->count() > 0){
                    Flash::warning($failcount . ' E-Mail(s) konnte(n) nicht versendet werden, bitte prüfen!');
                }*/
            } finally {
                // check states to verify emails have been sent
                /*if($failcount = IM::where('state_id', 1)->count() > 0){
                    Flash::warning($failcount . ' E-Mail(s) konnte(n) nicht versendet werden, bitte prüfen!');
                }else{
                    Flash::success('E-Mail-Versand an '. $guest_count . ' Kontakte erfolgreich in Auftrag gegeben!');
                }*/
                Flash::success('E-Mail-Versand an '. $invitations_count . ' Kontakte erfolgreich in Auftrag gegeben!');
            }
            Flash::success('E-Mail-Versand an '. $invitations_count . ' Kontakte erfolgreich in Auftrag gegeben!');
        }
    }

    public function onSendSelected($event_id){
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            $invitations = IM::where('event_id', $event_id)->whereIn('customer_id', $checkedIds)->get();

            foreach ($invitations as $invitation) {
                $this->pushMail($invitation->id, post('type'));
            }
            Flash::success('E-Mail-Versand an '. count($checkedIds) . ' Kontakte erfolgreich in Auftrag gegeben!'); //count($checkedIds)
        }else{
            Flash::warning('Es wurde nichts ausgewählt!');
        }
    }

    private function pushMail($invitation_id, $type){
        //$this->queueEmail(["invitee" => $invitee, "type" => post('type')]);

        Queue::push('Tripplefix\EventManager\Classes\SendEmail', ["invitation_id" => $invitation_id, "type" => $type]);
    }

    public function relationExtendConfig($config, $field, $model)
    {
        $config->view['list'] = '$/tripplefix/eventmanager/models/customer/columns_event.yaml';
    }
}
